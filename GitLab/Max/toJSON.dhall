let Prelude = ../Prelude.dhall

let JSON = Prelude.JSON

let Max = ./Type.dhall

let Max/toJSON
    : Max -> JSON.Type
    = \(max : Max) -> JSON.natural (merge { `0` = 0, `1` = 1, `2` = 2 } max)

in  Max/toJSON
