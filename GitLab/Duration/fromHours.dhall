let fromHours
    : Natural -> ./Type.dhall
    = \(n : Natural) -> ./fromMinutes.dhall (60 * n)

in  fromHours
