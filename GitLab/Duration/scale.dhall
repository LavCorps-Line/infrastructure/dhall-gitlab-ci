let Duration = ./Type.dhall

let scale
    : Natural -> Duration -> Duration
    = \(n : Natural) -> \(d : Duration) -> { seconds = d.seconds * n }

in  scale
