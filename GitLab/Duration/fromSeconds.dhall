let fromSeconds
    : Natural -> ./Type.dhall
    = \(n : Natural) -> { seconds = n }

in  fromSeconds
