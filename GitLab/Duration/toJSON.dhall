let Prelude = ../Prelude.dhall

let JSON = Prelude.JSON

let Duration = ./Type.dhall

let formatDur
    : Duration -> Text
    = \(dur : Duration) -> "${Natural/show dur.seconds} second"

let Duration/toJSON
    : Duration -> JSON.Type
    = \(dur : Duration) -> JSON.string (formatDur dur)

in  Duration/toJSON
