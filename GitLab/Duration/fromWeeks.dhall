let fromWeeks
    : Natural -> ./Type.dhall
    = \(n : Natural) -> ./fromDays.dhall (7 * n)

in  fromWeeks
