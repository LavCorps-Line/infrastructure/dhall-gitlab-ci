let fromYears
    : Natural -> ./Type.dhall
    = \(n : Natural) -> ./fromDays.dhall (365 * n)

in  fromYears
