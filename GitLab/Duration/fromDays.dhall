let fromDays
    : Natural -> ./Type.dhall
    = \(n : Natural) -> ./fromHours.dhall (24 * n)

in  fromDays
