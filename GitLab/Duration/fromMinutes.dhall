let fromMinutes
    : Natural -> ./Type.dhall
    = \(n : Natural) -> ./fromSeconds.dhall (60 * n)

in  fromMinutes
