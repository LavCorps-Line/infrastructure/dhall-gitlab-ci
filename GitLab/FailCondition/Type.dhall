< Always
| UnknownFailure
| ScriptFailure
| ApiFailure
| StuckOrTimeoutFailure
| RunnerSystemFailure
| RunnerUnsupported
| StaleSchedule
| JobExecutionTimeout
| ArchivedFailure
| UnmetPrerequisites
| SchedulerFailure
| DataIntegrityFailure
>
