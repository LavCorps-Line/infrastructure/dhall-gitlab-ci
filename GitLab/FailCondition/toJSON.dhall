let Prelude = ../Prelude.dhall

let JSON = Prelude.JSON

let FailCondition = ./Type.dhall

let FailCondition/toJSON
    : FailCondition -> JSON.Type
    = \(failCondition : FailCondition) ->
        JSON.string
          ( merge
              { Always = "always"
              , UnknownFailure = "unknown_failure"
              , ScriptFailure = "script_failure"
              , ApiFailure = "api_failure"
              , StuckOrTimeoutFailure = "stuck_or_timeout_failure"
              , RunnerSystemFailure = "runner_system_failure"
              , RunnerUnsupported = "runner_unsupported"
              , StaleSchedule = "stale_schedule"
              , JobExecutionTimeout = "job_execution_timeout"
              , ArchivedFailure = "archived_failure"
              , UnmetPrerequisites = "unmet_prerequisites"
              , SchedulerFailure = "scheduler_failure"
              , DataIntegrityFailure = "data_integrity_failure"
              }
              failCondition
          )

in  FailCondition/toJSON
