let FailCondition = ../FailCondition/Type.dhall

let Max = ../Max/Type.dhall

in  { max = Some Max.`0`, when = [] : List FailCondition } : ./Type.dhall
