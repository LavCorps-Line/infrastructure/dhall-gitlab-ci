let Prelude = ../Prelude.dhall

let Map = Prelude.Map

let JSON = Prelude.JSON

let Retry = ./Type.dhall

let Max = ../Max/Type.dhall

let Max/toJSON = ../Max/toJSON.dhall

let FailCondition = ../FailCondition/Type.dhall

let FailCondition/toJSON = ../FailCondition/toJSON.dhall

let dropNones = ../utils/dropNones.dhall

let Optional/map = Prelude.Optional.map

let Retry/toJSON
    : Retry -> JSON.Type
    = \(retry : Retry) ->
        let failConditionArrayJSON
            : List FailCondition -> JSON.Type
            = \(xs : List FailCondition) ->
                JSON.array
                  ( Prelude.List.map
                      FailCondition
                      JSON.Type
                      FailCondition/toJSON
                      xs
                  )

        let everything
            : Map.Type Text (Optional JSON.Type)
            = toMap
                { max = Optional/map Max JSON.Type Max/toJSON retry.max
                , when =
                    if    Prelude.List.null FailCondition retry.when
                    then  None JSON.Type
                    else  Some (failConditionArrayJSON retry.when)
                }

        in  JSON.object (dropNones Text JSON.Type everything)

in  Retry/toJSON
