let Max = ../Max/Type.dhall

let FailCondition = ../FailCondition/Type.dhall

in  { max : Optional Max, when : List FailCondition }
