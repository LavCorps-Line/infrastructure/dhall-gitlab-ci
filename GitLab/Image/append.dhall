let Image = ./Type.dhall

let mergeOptionalList = ../utils/mergeOptionalList.dhall

let append
    : Image -> Image -> Image
    = \(a : Image) ->
      \(b : Image) ->
        { name = b.name
        , entrypoint = mergeOptionalList Text a.entrypoint b.entrypoint
        }

in  append
