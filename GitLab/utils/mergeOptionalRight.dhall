-- Merge optionals. When both are Some, prefer the right one
let mergeOptional = ./mergeOptional.dhall

let mergeOptionalRight
    : forall (a : Type) -> Optional a -> Optional a -> Optional a
    = \(ty : Type) ->
      \(l : Optional ty) ->
      \(r : Optional ty) ->
        mergeOptional ty (\(_ : ty) -> \(x : ty) -> x) l r

in  mergeOptionalRight
