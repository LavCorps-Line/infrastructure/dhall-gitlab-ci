let mergeOptional
    : forall (a : Type) ->
      (a -> a -> a) ->
      Optional a ->
      Optional a ->
        Optional a
    = \(ty : Type) ->
      \(combine : ty -> ty -> ty) ->
      \(l : Optional ty) ->
      \(r : Optional ty) ->
        let onlyR = r

        let onlyL = l

        in  merge
              { None = onlyR
              , Some =
                  \(lSome : ty) ->
                    merge
                      { None = l
                      , Some = \(rSome : ty) -> Some (combine lSome rSome)
                      }
                      r
              }
              onlyL

in  mergeOptional
