-- Merge optionals. When both are Some, merge the lists
let mergeOptional = ./mergeOptional.dhall

let mergeOptionalList
    : forall (a : Type) ->
      Optional (List a) ->
      Optional (List a) ->
        Optional (List a)
    = \(ty : Type) ->
      \(l : Optional (List ty)) ->
      \(r : Optional (List ty)) ->
        mergeOptional (List ty) (\(a : List ty) -> \(b : List ty) -> a # b) l r

in  mergeOptionalList
