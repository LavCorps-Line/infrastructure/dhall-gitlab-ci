let Prelude = ../Prelude.dhall

let optionalList
    : forall (a : Type) -> Optional (List a) -> List a
    = \(a : Type) ->
      \(l : Optional (List a)) ->
        Prelude.List.concat a (Prelude.Optional.toList (List a) l)

in  optionalList
